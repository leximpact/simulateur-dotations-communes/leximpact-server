reforme_PLF_2023 = {
    "impot_revenu": {
        "bareme_ir_depuis_1945": {
            # « 10 225 € » est remplacé par le montant : « 10 777 € »
            # « 26 070 € » est remplacé par le montant :« 27 478 € »
            # « 74 545 € » est remplacé par le montant :« 78 570 € »
            # « 160 336 € » est remplacé par le montant : « 168 994 € »
            "seuils": [10_777, 27_478, 78_570, 168_994],
            "taux": [0.11, 0.3, 0.41, 0.45],
        },
        "calcul_impot_revenu": {
            "plaf_qf": {
                # « 790 € » et « 1 307 € » sont remplacés, respectivement, par les montants : « 833 € » et« 1 378 € »
                "decote": {"seuil_celib": 833, "seuil_couple": 1378, "taux": 0.4525},
                # « 951 € » est remplacé par le montant : « 1 002 € »
                "celib": 1_002,
                # « 3 756 € » est remplacé par le montant :« 3 959 € »
                "celib_enf": 3_959,
                # « 1 592 € » est remplacé par le montant : « 1 678 € »
                "general": 1_678,
                # « 1 587 € » est remplacé par le montant : « 1 673 €
                "reduc_postplafond": 1_673,
                # « 1 772 € » est remplacé par le montant : « 1 868 € »
                "reduc_postplafond_veuf": 1_868,
                "abat_dom": {
                    # Non revalorisé depuis 2018
                    "plaf_GuadMarReu": 2450,
                    "plaf_GuyMay": 4050,
                    "taux_GuadMarReu": 0.3,
                    "taux_GuyMay": 0.4,
                },
            },
            # « 6 042 € » est remplacé par le montant :« 6 368 € » - https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000044978337
        }
    }
}
