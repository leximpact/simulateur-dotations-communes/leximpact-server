reforme_PLF_2022 = {
    "impot_revenu": {
        "bareme_ir_depuis_1945": {
            "seuils": [10225, 26070, 74545, 160336],
            "taux": [0.11, 0.3, 0.41, 0.45],
        },
        "decote": {"seuil_celib": 790, "seuil_couple": 1307, "taux": 0.4525},
        "plaf_qf": {
            "abat_dom": {
                "plaf_GuadMarReu": 2450,
                "plaf_GuyMay": 4050,
                "taux_GuadMarReu": 0.3,
                "taux_GuyMay": 0.4,
            },
            "celib": 951,
            "celib_enf": 3756,
            "general": 1592,
            "reduc_postplafond": 1587,
            "reduc_postplafond_veuf": 1772
        },
    }
}
