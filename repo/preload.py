from models import to_postgres
import os
import glob
from pathlib import Path

tmp_upload_folder = os.path.abspath("../leximpact-server-uploads")
print(f"Looking into {tmp_upload_folder}")
files = glob.glob(tmp_upload_folder + '/*.csv')
files += glob.glob(tmp_upload_folder + '/*.h5')
csvs = [f for f in files if f.split(".")[-1] in ("csv", "h5")]
print("csv/h5 files :", csvs)
for csv in csvs:
    path_file = csv
    table_name = Path(path_file).resolve().stem
    print(f"Uploading {csv} to {table_name}")
    to_postgres(path_file, table_name, if_exists="replace")
    print("Upload done, please delete the files")
