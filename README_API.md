# Routes de l'API Web

Cette documentation présente les endpoints de l'API Web de leximpact-server pour :
* l'[application d'impôt sur le revenu](http://leximpact.an.fr/ir)
* et l'[application des dotations](http://leximpact.an.fr/dotations).

## Endpoints de l'API Web - Impôt sur le revenu

L'API Web dispose des itinéraires suivants :

* `/`
* `/metadata/description_cas_types`
* `/auth/login`
* `/calculate/compare`
* `/calculate/simpop`

Parmi ces itinéraires, deux nécessitent une vérification de l'identité de l'appelant :

* `/auth/login` : vérifie que l'email mentionné dans le corps est dans la base de données (si oui, lui envoie par mail un lien comportant un token)
* `/calculate/simpop` : vérifie que le token présent dans le corps de la requête est valide, non expiré, et n'appartient pas à un utilisateur suspendu
* `/search?commune=substring` : Renvoie une liste de communes contenant la chaine de caractères spécifiée

###  Itinéraire par défaut ou /

- Type : GET
- Description : Permet de vérifier que le serveur est en fonctionnement.
- Requête - contenu du body : None
- Réponse - contenu du body :

    ```json
    { "hello": "coucou" }
    ```

### /metadata/description_cas_types

- Type : POST
- Description :  Requête le serveur pour obtenir le contenu des cas types par défaut. Permet d'éviter de stocker le contenu de ces cas types dans le client.
- Requête - contenu du body : None
- Réponse - contenu du body : `Array of objects`. Chaque objet décrit un foyer fiscal sous forme de nombre entier à travers le schéma suivant :

    ```js
    {
        "declarants": [ // tableau contenant un élément par déclarant du foyer fiscal
            {
                "ancienCombattant": false,  // décrit si le déclarant est ancien combattant
                "invalide": false, // décrit si le déclarant est en situation d'invalidité
                "parentIsole": false, // décrit si le déclarant est parent isolé
                "retraite": false, // décrit si le déclarant est retraité
                "veuf": false // décrit si le déclarant est veuf
            }
        ],
        "personnesACharge": [ // tableau contenant un élément par personne à charge du foyer fiscal
            {
                "chargePartagee": false,  // décrit si la personne à charge est en charge partagée
                "invalide": false  // décrit si la personne à charge est en situation d'invalidité
            }
        ],
        "residence": "metropole",   // décrit les différentes situations de résidence :"GuadMarReu" pour Guadeloupe/Martinique/Réunion, ou "GuyMay"  pour Mayotte/Guyane, "metropole" pour le reste du territoire français
        "revenuImposable": 54003 //en euros imposable pour l'ensemble du foyer fiscal
    }
    ```

###  /auth/login

- Type : POST
- Description : Soumet un email au serveur. Si cet email est dans la liste des adresses autorisées, un lien de connexion au service LexImpact IR à accès restreint est envoyé à l'adresse email spécifiée.
- Requête - contenu du body : Ne contient qu'un champ tel que décrit dans l'exemple suivant.

    ```json
    {
        "email" : "email@tester.io"
    }
    ```

- Réponse - contenu du body :  La réponse renvoyée sera toujours la même, afin d'éviter de donnner des informations sur la validité des adresses mail : juste une chaîne de caractères qui contient `Bien reçu! Si l'email est valide, nous avons envoyé un mail de confirmation`.

###  /calculate/compare

- Type : POST
- Description : On décrit au serveur une description de cas-types, et une réforme, et ce dernier nous renvoie l'effet de la réforme sur ces cas-types.
- Requête - contenu du body :

    ```js
    {
        "reforme" : décrit la réforme,
        "deciles": deprecated - n'a plus d'impact,
        "description_cas_types": array de descriptions de cas-types (pour la structure, cf. le guide du endpoint
        /metadata/description_cas_types) ; champ optionnel, si non fourni, utilise les descriptions de cas types par défaut,
        "timestamp" : chaîne de caractères qui sera renvoyé tel quel par le programme ; champ optionnel, si non fourni, la réponse ne contiendra pas de champ "timestamp"
    }
    ```

En version `1.1.0` de la [spécification de l'API Web](./server/api.yaml), la structure de la réforme étend les possibilités d'amendement du quotient familial et cela est défini par un nouveau champ `calculNombreParts`.

Elle reproduit presque la structure d'OpenFisca-France. Si un paramètre est omis, il est remplacé par la version par défaut d'OpenFisca-France (donc le code de loi existant). Le champ `calculNombreParts` est optionnel, mais s'il figure, tous ses champs doivent y figurer, et les élément du tableau associés aux quatre situations familiales doivent faire la même longueur :


```json
"reforme" : {
    "impot_revenu" :{
        "bareme_ir_depuis_1945" :{
            "seuils": [9964, 25659, 73369, 157806],
            "taux": [0.14, 0.3, 0.41, 0.45]
        },
        "decote" : {"seuil_celib": 777, "seuil_couple": 1286, "taux": 0.4525},
        "plaf_qf":{
            "abat_dom":{
                "taux_GuadMarReu" : 0.3,
                "plaf_GuadMarReu" : 2450,
                "taux_GuyMay" : 0.4,
                "plaf_GuyMay" : 4050
            },
            "maries_ou_pacses" : 1567,
            "celib_enf" : 3697,
            "celib" : 936,
            "reduc_postplafond" : 1562,
            "reduc_postplafond_veuf": 1745,
            "reduction_ss_condition_revenus" :{
                "seuil_maj_enf": 3797,
                "seuil1": 18985,
                "seuil2":21037,
                "taux":0
            }
        },
        "calculNombreParts": {
            "partsSelonNombrePAC": [
                {
                    "veuf": 1,
                    "mariesOuPacses": 2,
                    "celibataire": 1,
                    "divorce": 1
                },
                {
                    "veuf": 2.5,
                    "mariesOuPacses": 2.5,
                    "celibataire": 1.5,
                    "divorce": 1.5
                },
                {
                    "veuf": 3,
                    "mariesOuPacses": 3,
                    "celibataire": 2,
                    "divorce": 2
                },
                {
                    "veuf": 4,
                    "mariesOuPacses": 4,
                    "celibataire": 3,
                    "divorce": 3
                }
            ],
            "partsParPACAuDela": 1,
            "partsParPACChargePartagee": {
                "zeroChargePrincipale": {
                    "deuxPremiers": 0.25,
                    "suivants": 0.5
                },
                "unChargePrincipale": {
                    "premier": 0.25,
                    "suivants": 0.5
                },
                "deuxOuPlusChargePrincipale": {
                    "suivants": 0.5
                }
            },
            "bonusParentIsole": {
                "auMoinsUnChargePrincipale": 0.5,
                "zeroChargePrincipaleUnPartage": 0.25,
                "zeroChargeprincipaleDeuxOuPlusPartage": 0.5
            }
        }
    }
}
```
Où `PAC` désigne `personne à charge`.

- Réponse - contenu du body :
  - res_brut : Impôts payés par les cas-types :
    - res_brut.avant : Impôt payé avec le code existant
    - res_brut.plf : Impôt payé avec le PLF
    - res_brut.apres : Impot payé avec la réforme spécifiée par la requête.
  - nbreParts : Nombre de parts fiscales des cas-types :
    > Le champ `nbreParts` est ajouté à la réponse en version `1.2.0`.
    - nbreParts.avant : Nombre de parts avec le code existant
    - nbreParts.plf : Nombre de parts avec le PLF
    - nbreParts.apres : Nombre de parts avec la réforme spécifiée par la requête.
  - timestamp : Chaîne de caractères reçue dans la requête
  - total : somme des impôts payés par les cas-types dans les trois scénarios. Inutile pour cette requête.

    ```json
    {
       "res_brut":{
          "apres":{
             "0":0,
             "1":-1839,
             "2":-1292,
             "3":0,
             "4":-2545,
             "5":-1206
          },
          "avant":{
             "0":0,
             "1":-1839,
             "2":-1292,
             "3":0,
             "4":-2545,
             "5":-1206
          },
          "plf":{
             "0":0,
             "1":-1297,
             "2":-1020,
             "3":0,
             "4":-1655,
             "5":-772
          }
       },
        "nbreParts":{
          "apres": {
            "0": 1.0,
            "1": 1.5,
            "2": 2.0,
            "3": 2.0,
            "4": 3.0,
            "5": 3.0
            },
          "avant": {
            "0": 1.0,
            "1": 1.5,
            "2": 2.0,
            "3": 2.0,
            "4": 3.0,
            "5": 3.0
            },
          "plf": {
            "0": 1.0,
            "1": 1.5,
            "2": 2.0,
            "3": 2.0,
            "4": 3.0,
            "5": 3.0
            }
       },
       "timestamp":"1234564689",
       "total":{
          "apres":6882.0,
          "avant":6882.0,
          "plf":4744.0
       }
    }
    ```

### /calculate/simpop

- Type : POST
- Description : On décrit au serveur une réforme. On s'authentifie par le `token` qui nous a été fourni dans le mail d'authentification/login. Le serveur renvoie l'impact de la réforme sur la population : recettes totales, recettes par décile de RFR (Revenu Fiscal de Référence), frontières entre les déciles, nombre de foyers fiscaux touchés.
- Requête - contenu du body :

    ```js
    {
        "reforme" : décrit la réforme au même format que la réforme de l'itinéraire /calculate/compare,
        "timestamp" : chaîne de caractères qui sera renvoyée telle quelle par le programme.
            Champ optionnel, si non fourni, la réponse ne contiendra pas de champ "timestamp",
        "token" : le token d'authentification temporaire qui a été fourni dans l'email.
    }
    ```

- Réponse - contenu du body :
  - timestamp : chaîne de caractères reçue dans la requête
  - total : somme des impots payés par la population dans les trois scénarios.
  - frontieres_deciles : limites supérieures des RFR des 10 déciles de foyers fiscaux (classés par RFR total par foyer fiscal)
  - foyers_fiscaux_touches : dictionnaire contenant les clefs `avant_to_plf`, `avant_to_apres`, `plf_to_apres`. Chaque élément du dictionnaire divise les foyers fiscaux en 5 catégories : `gagnant`, `perdant`, `neutre`, `neutre_zero`, `perdant_zero` : Ils décrivent si les foyers fiscaux payent plus ou moins d'impôts à l'arrivée qu'au départ. Les catégories finissant par `_zero` décrivent le foyers fiscaux qui étaient exonérés d'impôt au départ.
 Par exemple, `neutre_zero` désigne le nombre de personnes pour lesquelles l'impact est neutre (avant = après) et qui sont exonérées d'impôt (donc, avant = après = 0).


## Endpoints de l'API Web - Dotations aux collectivités locales

L'API Web traite également d'une thématique isolée de l'impôt sur le revenu : les dotations
de l'État aux collectivités locales.

### /dotations

- Type : POST
- Description :
- Requête - contenu du body :
  > En cours de définition. Valeur par défaut :
  ```json
  {
    "reforme": {  # décrit la réforme
        "dotations": {  # configure les éléments de la réforme des dotations
            "montants": {"dgf": montant de la DGF},
            "communes": {
                "dsr": {},
                "dsu": {}
            }
        }
    },
    "strates": [...]
  }
  ```
- Réponse - contenu du body :
  > En cours de définition. Valeur par défaut :
  ```json
  {
    "amendement": {
        "communes": {
            "dsr": {},
            "dsu": {}
          }
      },
      "base": {
        "communes": {
            "dsr": {},
            "dsu": {}
          }
      },
      "baseToAmendement": {
            "communes": {
                "dsr": {},
                "dsu": {}
          }
      }
  }
```
