#!/usr/bin/env python
# coding: utf-8

# '''
# Objet du script: Attribuer un Rk à chaque foyer fiscal en fonction de son RFR
# '''

import pandas as pd  # type: ignore
from random import random
# Load OpenFisca
from Simulation_engine.simulate_pop_from_reform import (
    simulation,
    TBS_DEFAULT,
    annee_de_calcul,
)  # type: ignore


# On charge notre distribution de RFR et de Rk
with open("./Simulation_engine/ExportCopule.txt", "r") as fichier:
    contenu = fichier.read()
    dictionnaire_fichier = eval(contenu)
"""
lower_bound : revenu minimum de RFR
upper_bound : revenu maximum de RFR
nb_people : { 'zero' : Nb foyers tels que Rk==0 , 'nonzero': Nb foyers tels que Rk > 0 }
nonzerobucket : Tableau de couple [nb foye] du bucket, Seuil plafond du bucket
"""
print(dictionnaire_fichier[0])
print("-" * 50)
print(dictionnaire_fichier[5])

# foyers = 0
# somme = 0
# for b in dictionnaire_fichier:
#     for rk in b['nonzerobuckets']:
#         foyers += rk[0]
#         somme += rk[1]
#     foyers += b['nb_people']['zero']
#     print("compa sommes : ", b['nb_people']['zero'] , sum([rk[0] for rk in b["nonzerobuckets"]]))
# print(f'Somme des revenus du capital : {somme / 1e9:.2f}')
# print(f'Nombre de foyers : {foyers}')

calib_rk = pd.read_csv("./Simulation_engine/CalibPOTE_rev_capital_partiel.txt")
# La première ligne suffit pour retrouver la somme

# print('Nb foy dans CalibPOTE_rev_capital_partiel : ', calib_rk['Nk'].sum())
# print('Somme revKpi dans CalibPOTE_rev_capital_partiel : ', calib_rk['Ark'].sum())
# calib_rk['somme'] = calib_rk['Nk'] * calib_rk['Ark'] * 38_000_000
# somme_rk = calib_rk['somme'].sum()
# print(somme_rk)


# # Implementation d'une fonction probabilistique qui associe un Rk estimé à un RFR donné
def revenu_du_capital_estime(rfr):
    # Dans quel bucket est rfr? ===> Nous donne la distrib du Rk
    # rfr :  un nombre
    # dictionnaire_fichier

    for bucket in dictionnaire_fichier:
        if bucket["lower_bound"] <= rfr < bucket["upper_bound"]:
            break

    probazero = bucket["nb_people"]["zero"] / sum(bucket["nb_people"].values())
    # {'lower_bound': 0, 'upper_bound': 1.0, 'nb_people': {'zero': 2267320, 'nonzero': 33668}
    # 2267320 /(2267320+33668) = 98.5%

    # On genere une variable aleatoire entre 0 et 1
    randunif = random()
    # Si cette proba est <= à la probabilité que Rk = 0
    if randunif <= probazero:
        return 0
    # Si on n'est pas dans ce cas
    randunif = (randunif - probazero) / (1 - probazero)

    randinnbgens = randunif * bucket["nb_people"]["nonzero"]

    # Générer un rk à partir de cette distrib (Monte-Carlo probablement)
    sommepass = 0  # nombre de gens dans les buckets déjà dépassés. Quand on a passé le nombre de gens qu'on veut, on a fini de générer !
    for bucket_rk in bucket["nonzerobuckets"]:
        sommepass += bucket_rk[0]
        if sommepass > randinnbgens:
            return bucket_rk[1] / bucket_rk[0]


# # On vérifie l'exactitude de notre fonction sur la répartition des gens de Rk = 0
nbpopo = 0
nbtot = 10000
for _i in range(nbtot):
    rfr = 10000
    rce = revenu_du_capital_estime(rfr)
    # print(f"J'ai genere {rce} pour un random ff qui aurait {rfr} de revenu")
    if rce > 0:
        nbpopo += 1

print(
    f"Au total, j'ai {nbpopo/nbtot * 100:.2f}% non nuls, je devrais en avoir {100713/(100713 + 284008) *100:.2f}% "
)

# # On va ajouter cette colonne de Rk dans le dummy_data_final (par individu)
dummy_data_final = pd.read_hdf(
    "./Simulation_Engine/dummy_data_final.h5"
)  # Une ligne par personne
# print(dummy_data_final.columns)

# On ne peut calculer le RFR que par foyer fiscal
# On charge une simulation OpenFisca
my_simu, dico = simulation(
    data=dummy_data_final, tbs=TBS_DEFAULT["avant"], period=annee_de_calcul
)
# print(my_simu.calculate('rfr', '2020'))
# On regroupe les 117 000 lignes (d'individus) de dummy_data_final en 58 000 lignes de foyers fiscaux
ddf = dummy_data_final[["quifoy", "idfoy", "wprm"]]
dd_ff = ddf.groupby(["idfoy", "wprm"], as_index=False).sum()

to_calc = my_simu.calculate("rfr", annee_de_calcul)
dd_ff["rfr"] = to_calc
# print(dd_ff.columns)
# print(dd_ff)

# On génère la colonne 'rk' dans dd_ff
dd_ff["rk"] = dd_ff["rfr"].apply(lambda row: revenu_du_capital_estime(row))
print(dd_ff)
# print(dd_ff.columns)

# Vérification de l'agrégat : on veut 75 Mds€
dd_ff["rk*wprm"] = dd_ff["rk"] * dd_ff["wprm"]
print("Agrégat pondéré de Rk, wanted 75Mds€", dd_ff["rk*wprm"].sum())

# On exporte les données (groupées par foyer fiscal)
dd_ff.to_csv("dummy_data_foy_rk.csv")

# # On split dd_ff sur les individus:
#
# ## 1 - On attribue le Rk d'un foyer fiscal à toutes les personnes du foyer dans dummy_data_final
# On ne garde que les colonnes d'intérêt:
# dummy_data_final[['quifoy','idfoy']].head()
# dd_ff2.head()
dd_ff2 = dd_ff[["idfoy", "rk", "rfr"]]
dummy_data_final_rk = pd.merge(
    dummy_data_final, dd_ff2, left_on="idfoy", right_on="idfoy", indicator=True
)

# print('Colonnes après merge', dummy_data_final_rk.columns)
# dummy_data_final_rk[['quifoy', 'idfoy', 'rk']].head()

# ## 2 - On supprime le Rk aux personnes autres que le déclarant principal du foyer fiscal (i.e. quifoy=0)
print(dummy_data_final_rk.columns)
dummy_data_final_rk.loc[dummy_data_final_rk["quifoy"] != 0, "rk"] = 0
dummy_data_final_rk[["quifoy", "idfoy", "rk", "rfr"]].head()

# Vérification de l'aggrégat : on veut 75 Mds€
dummy_data_final_rk["rk*wprm"] = dummy_data_final_rk["rk"] * dummy_data_final_rk["wprm"]

print("Agrégat pondéré de Rk avant merge, wanted 75Mds€", dd_ff["rk*wprm"].sum())
print("Agrégat pondéré de Rk, wanted 75Mds€ : ", dummy_data_final_rk["rk*wprm"].sum())
print("Nombre de lignes, wanted 116861 : ", len(dummy_data_final_rk))

dd_ff[["quifoy", "idfoy", "rk", "wprm", "rk*wprm"]].head(10)
dummy_data_final_rk[["quifoy", "idfoy", "rk", "wprm", "rk*wprm"]].head(10)

# On exporte les données
dummy_data_final_rk.to_csv("dummy_data_ind_rk.csv")


# On calcule l'assiette CSG pour appliquer le taux
dummy_data_csg = dummy_data_final_rk[
    [
        "age",
        "categorie_salarie",
        "chomage_brut",
        "contrat_de_travail",
        "heures_remunerees_volume",
        "idfam",
        "idfoy",
        "idmen",
        "pensions_alimentaires_percues",
        "quifam",
        "quifoy",
        "retraite_brute",
        "statut_marital",
        "salaire_de_base",
        "taux_csg_remplacement",
        "f4ba",
        "wprm",
        "rfr",
    ]
]
# assiette_csg_revenus_capital = dummy_data_final_rk['f4ba'] + dummy_data_final_rk['rk']

# On a aggrégé ces revenus dans:
revenus_capital = dummy_data_final_rk["rk"]
# On voudrait faire:
# dummy_data_csg['revenus_capital'] = dummy_data_final_rk['rk']

# Or la variable 'revenus_capital' n'est pas lue par OF, mais ses sous-variables le sont.
# On les définis donc arbitrairement comme:
dummy_data_csg["revenus_capitaux_prelevement_bareme"] = revenus_capital / 3
dummy_data_csg["revenus_capitaux_prelevement_liberatoire"] = revenus_capital / 3
dummy_data_csg["revenus_capitaux_prelevement_forfaitaire_unique_ir"] = (
    revenus_capital / 3
)

# On calcule la CSG
# On charge une simulation OpenFisca
my_simu2, dico = simulation(
    data=dummy_data_csg, tbs=TBS_DEFAULT["avant"], period=annee_de_calcul
)

# assiette_csg_ff = my_simu2.calculate('assiette_csg_revenus_capital', annee_de_calcul)
# dummy_data_csg["assiette_csg_revenus_capital"] = to_calc2 --> non car ça passe en foyers fiscaux
# print(dummy_data_csg.columns)
# dummy_data_csg.head()

# Calcul de la csg
to_calc3 = my_simu2.calculate("csg_revenus_capital", annee_de_calcul)
print(to_calc3)
print("CSG avant: ", to_calc3.sum())
CSG_Rk = to_calc3 * dd_ff["wprm"]

print("CSG pondérée, expected ~10 Mds€ ", CSG_Rk.sum())
# Ref: p51 de https://www.securite-sociale.fr/files/live/sites/SSFR/files/medias/CCSS/2019/CCSS%20SEPT%2019%20DEF.pdf
