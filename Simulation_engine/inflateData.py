import pandas as pd  # type: ignore
import numpy as np  # type: ignore
import os


def inflate(inputfile, outputfile=None):
    if outputfile is None:
        outputfile = inputfile
    df = pd.read_hdf(inputfile)
    final = df.copy()
    columns = df.columns
    print(columns)
    print(df)

    # Inflation économique
    startp = int(os.getenv("YEAR_ERFS"))  # Année de la dernière version de l'ERFS-FPR
    endp = int(os.getenv("YEAR_COMPUTATION"))  # Année de production de la base -> on ne produit pas de base 2022, car en 2022 on aura les impots sur 2021

    # list of variables to inflate
    to_inflate = [
        "chomage_brut",
        "pensions_alimentaires_percues",
        "rag",
        "ric",
        "rnc",
        "salaire_de_base",
        "f4ba",
        "loyer",
        "taxe_habitation",
    ]

    # Méthode
    """
        Afin d'appliquer l'inflation sur les salaires, on a deux options :
            - appliquer l'inflation (ou indice du prix à la consommation dans le PLF) (hors tabac) que l'on peut trouver dans le PLF
            - faire un calcul plus poussée en faisant une approximation de l'inflation salariale à partir des agrégats de salaires des années précédentes
        Nous décidons de garder ici les données pour faire l'un ou l'autre, mais nous appliquons la 2e méthode.
    """
    # METHODE 1
    # Création de la liste des taux d'inflation (en %)
    # Données issues du PLF - 'Chiffres clés' (=inflation hors tabac = prix à la consommation)

    # inflation = {
    #     "2017": 1.0,
    #     "2018": 1.6,
    #     "2019": 0.9,
    #     "2020": 0.2,
    #     "2021": 1.4,
    #     "2022": 1.5,  # Estimation PLF 2022
    # }
    # Calcul du taux d'inflation sur plusieurs années
    # adjrate = 1
    # for year in range(startp, endp):
    #     rateinfla = inflation[str(year)]
    #     adjrate = adjrate * (1 + rateinfla/100)  # Car on a les données en %
    # print("Taux d'ajustement : ", adjrate)

    # METHODE 2
    # Source : https://www.impots.gouv.fr/portail/statistiques > Impot des Particuliers > Par région > National.xls
    #   > Traitements et salaires > Montant
    total_annuel_salaires = {
        "2016": 683_665_874,
        "2017": 695_665_787,
        "2018": 713_523_524,
        "2019": 738_077_177,
        "2020": 743_185_656,
    }

    # Calcul du taux d'inflation sur plusieurs années
    adjrate = 1
    # Taux d'inflation des années connues
    last_value_connnue = int(list(total_annuel_salaires.keys())[-1])  # 2020 : Derniere date de la valeur connue
    for year in range(startp, last_value_connnue):
        rateinfla_annuel = total_annuel_salaires[str(year + 1)] / total_annuel_salaires[str(year)]
        print(year , rateinfla_annuel)
        adjrate = adjrate * rateinfla_annuel

    # Projection pour les années manquantes
    adjrate = adjrate ** ((endp - startp + 1) / (last_value_connnue - startp + 1))
    print("Taux d'ajustement : ", adjrate)

    print('😈Sum salaire de base avant inflation ', (final['salaire_de_base'] * final['wprm']).sum())
    for vartoinflate in to_inflate:
        if vartoinflate in final.columns:
            final[vartoinflate] = final[vartoinflate] * adjrate
    print('😈Sum salaire de base apres inflation ', (final['salaire_de_base'] * final['wprm']).sum())

    # Source : https://www.impots.gouv.fr/portail/statistiques > Impot des Particuliers > Par région > National.xls
    # Inflation du nombre de foyers (! national.xls 2020 = Revenus 2019 -> "2019": Nb de foyers en 2019)
    nb_foyers_par_annee = {
        "2011": 36_389_256,
        "2012": 36_720_036,
        "2013": 37_119_219,
        "2014": 37_429_459,
        "2015": 37_683_595,
        "2016": 37_889_181,
        "2017": 38_332_977,
        "2018": 38_549_926,
        "2019": 39_331_689,
        "2020": 39_331_689 + (39_331_689 - 38_549_926),  # Hypothèse : évolution linéaire entre 2018 et 2020
        "2021": 39_331_689 + 1.5 * (39_331_689 - 38_549_926),  # Hypothèse: avec le Covid, on augmente, mais un peu moins vite qu'avant (1,5 écart en 2 ans)
        "2022": 39_331_689 + 1.7 * (39_331_689 - 38_549_926),  # Hypothèse: on suppose que les foyers n'ont quasi pas augmenté entre 2021 et 2022 (NB: inutile de chercher, on a décidé au doigt mouillé)
    }

    print(
        "Attention: \n "
        + "Hypothèse 2020 : on suppose une évolution linéaire entre 2018 et 2020 \n"
        + "Hypothèse 2021: avec le Covid, on augmente, mais un peu moins vite qu'avant (1,5 écart en 2 ans) \n"
        + "Hypothèse 2022: on suppose que les foyers n'ont quasi pas augmenté entre 2021 et 2022 (NB: inutile de chercher, on a décidé au doigt mouillé) \n \n")

    InflaFF = nb_foyers_par_annee[str(endp)] / nb_foyers_par_annee[str(startp)]
    print("Taux d'inflation ff: ", InflaFF)

    print('😈Sum salaire de base avant inflation FF ', (final['salaire_de_base'] * final['wprm']).sum())
    final["wprm"] = final["wprm"] * InflaFF
    print('😈Sum salaire de base apres inflation FF ', (final['salaire_de_base'] * final['wprm']).sum())
    final.to_hdf(outputfile, key="input")


def noise(inputfile, outputfile=None):  # add gaussian noise
    if outputfile is None:
        outputfile = inputfile
    df = pd.read_hdf(inputfile)
    columns = df.columns
    print(columns)
    print(df)
    # list of variables to gaussiannoise
    to_noise = [
        "chomage_brut",
        "pensions_alimentaires_percues",
        "rag",
        "ric",
        "rnc",
        "salaire_de_base",
        "f4ba",
        "loyer",
        "taxe_habitation",
    ]
    sigma = 0.02

    for var_noised in to_noise:
        if var_noised in df.columns:
            print("noising {}".format(var_noised))
            noise = np.random.normal(0, sigma, [2, 2])
            sig = df[var_noised]
            noise = np.random.lognormal(-sigma * sigma / 2, sigma, [len(df)])
            adjed = sig * noise
            print(sum(sig), sum(noise) / len(noise), sum(adjed))
            df[var_noised] = adjed
    import os

    print(inputfile, outputfile, os.listdir("."))
    df.to_hdf(outputfile, key="input")


if __name__ == "__main__":
    inflate("dummy_data.h5", "dummy_data_inflated.h5")
    noise("dummy_data_inflated.h5", "dummy_data_inflated_noised.h5")
