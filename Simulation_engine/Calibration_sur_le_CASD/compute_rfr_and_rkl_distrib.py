"""
Script pour l'extraction du RFR et du Rk dans le CASD
"""
import pandas as pd  # type: ignore
from time import time
import sys

starttime = time()
rfrs = pd.read_csv("RevenusSmall.csv")
print("Loaded csv", time() - starttime)

starttime = time()
nb_bucket_rk = 100  # rk : revenus du capital


class BucketDeVAR:
    """
    On crée une class pour gérer le contenu de chaque ligne=observation du tableau de Variable=RFR ou Rk ici
    """

    def __init__(self, lower_bound, upper_bound, liste_des_rk):
        self.lower_bound = lower_bound
        self.upper_bound = upper_bound
        self.rklist = sorted(liste_des_rk[:])  # On trie les rk par ordre croissant
        print("pff", len(self.rklist))
        self.nbpeople = len(self.rklist)
        self.nbzero = len(
            [rk for rk in self.rklist if rk < 0.01]
        )  # Nb de gens qui ont 0 de rk
        self.rknonzero = [rk for rk in self.rklist if rk >= 0.01]
        self.nbnz = len(self.rknonzero)  # Nb d'observations de valeur > 0

        # Découpage du RFR
        # On fait 100 paliers (de 1%) des gens au-dessus de zéro  en rk
        self.frontieres_gens = [
            int(self.nbnz * i / nb_bucket_rfr) for i in range(1, nb_bucket_rfr)
        ]
        # On vire les sous-buckets de moins de 12 personnes
        i = 0
        while i < len(self.frontieres_gens) - 1:
            if (
                self.frontieres_gens[i + 1] - self.frontieres_gens[i] <= 12
            ):  # Si on a moins de 12,
                # On combine ces deux frontières, en supprimant la frontière en cours
                self.frontieres_gens = (
                    self.frontieres_gens[:i] + self.frontieres_gens[i + 1 :]
                )
            else:
                i += 1
        # On refait une passe (pour les premières et dernières frontières qu'on aurait oubliées)
        self.frontieres_gens = [
            k for k in self.frontieres_gens if 12 <= k <= self.nbnz - 12
        ]
        # On ajoute une dernière frontière
        self.frontieres_gens += [self.nbnz]

        # Découpage du Rk (revenu du capital)
        # Initialisation
        currbuck = 0
        resultat = [[0, 0]]
        print(self.frontieres_gens)
        # On parcourt les revenus du capital > 0
        for idrk, rk in enumerate(self.rknonzero):
            # Si le nb d'observations est supérieur au plancher du current bucket
            if idrk >= self.frontieres_gens[currbuck]:
                resultat += [[0, 0]]  # On ajoute une liste vide
                print(
                    "J'ai changé de bucket,",
                    # Plancher du current bucket
                    self.frontieres_gens[currbuck],
                    # Plafond du current bucket si l'indice du bucket supérieur est <  nb de seuils
                    self.frontieres_gens[currbuck + 1]
                    if currbuck + 1 < len(self.frontieres_gens)
                    else None,
                    # Nb cumulés d'observations telles que Rk>0 et Rk< plafond(current bucket)
                    idrk,
                    # Nb d'observations dans ce bucket
                    resultat[-2],
                )
                currbuck += 1
            resultat[-1][0] += 1  # Nb d'observations dans le bucket
            resultat[-1][1] += rk  # Seuil plafond du bucket
        # Fonction de distribution du Rk
        self.distribnonzero = resultat
        print(self.lower_bound)
        print(self.upper_bound)
        print(self.nbpeople)
        print(self.nbzero)
        print(self.frontieres_gens)
        print(self.distribnonzero)

    def to_dict(self):
        d = {
            "lower_bound": self.lower_bound,
            "upper_bound": self.upper_bound,
            "nb_people": {"zero": self.nbzero, "nonzero": self.nbnz},
            "nonzerobuckets": self.distribnonzero,
        }
        return d


"""splitter le VAR (RFR) en m=100 buckets
Chacun de ces buckets est splitté en n=100 quantiles égaux de R Kl
Pour chaque bucket et quantile :
  - quel est le rk moyen de ce bucket?
Puis je tirerai au sort le Kl

"""

nb_bucket_rfr = 100

# On trie par revenus croissants
rfrs = rfrs.sort_values(by="revkire")
nb_lignes = len(rfrs)

# On calcule les seuils (d'observations) en découpant la population en n=100 groupes égaux
frontieres_gens = [int(nb_lignes * i / nb_bucket_rfr) for i in range(1, nb_bucket_rfr)]

# On récupère la liste de tous les revenus, foyer par foyer (38 millions)
tablo_valeurs = rfrs["revkire"].values

# On commence par un revenu à zéro, puis on récupère le revenu de la personne à la frontière
frontieres_valeurs = [0] + [
    tablo_valeurs[frontiere]
    for frontiere in frontieres_gens
    if tablo_valeurs[frontiere] > 0
]

# On initialise un compteur et un tableau de tableau
bucket_rfr_number = 0
list_revenus_each_bucket_temp = [[]]  # type: ignore
kk = 0

# On récupère toutes les colonnes du dataset, ligne par ligne (38 millions)
for _, rfr, rk in rfrs.values:
    # On change de bucket quand le rfr dépasse le montant de la frontière
    if (
        bucket_rfr_number < len(frontieres_valeurs) - 1
        and rfr >= frontieres_valeurs[bucket_rfr_number + 1] - 0.01
    ):
        # On crée une nouvelle liste pour un nouveau bucket
        bucket_rfr_number += 1
        print(
            "done",
            bucket_rfr_number,
            "bucket",
            kk,
            len(list_revenus_each_bucket_temp[-1]),
            len(rfrs),
        )
        list_revenus_each_bucket_temp += [[]]
    # Dans cette liste de bucket on mémorise le Rk de chaque foyer de ce bucket
    list_revenus_each_bucket_temp[-1] += [rk]
    kk += 1  # Nb total de foyer fiscaux (all buckets included)

list_revenus_each_bucket = [tuple(k) for k in list_revenus_each_bucket_temp]  # nomypy
# list_revenus_each_bucket = [tuple(k) for k in list_revenus_each_bucket]  # nomypy
frontieres_valeurs += [10 ** 15]  # On ajoute une dernière frontière à 10^15 €
print(kk)
print(
    [len(k) for k in list_revenus_each_bucket],
    sum([len(k) for k in list_revenus_each_bucket]),
)
# print(frontieres_gens)
# print(frontieres_valeurs)

# Initialisation
to_export = []

for i in range(len(list_revenus_each_bucket)):  # On parcourt les buckets de Rk
    print(
        frontieres_valeurs[i],  # Le seuil plancher de Rk de ce bucket
        len(list_revenus_each_bucket[i]),  # Nb d'observations dans ce bucket
        list_revenus_each_bucket[i][:10],  # Les 10 premiers Rk de ce bucket
        len(
            [k for k in list_revenus_each_bucket[i] if k < 0.01]
        ),  # Le Nb d'observations à Rk = zéro
        len([k for k in list_revenus_each_bucket[i] if k < 0.01])
        / len(
            list_revenus_each_bucket[i]
        ),  # La part d'observations : Rk = zéro dans ce bucket
    )
    print(
        [len(k) for k in list_revenus_each_bucket],
        sum([len(k) for k in list_revenus_each_bucket]),
    )
    print(
        "Ouhouhou",
        frontieres_valeurs[i],
        frontieres_valeurs[i + 1],
        len(list_revenus_each_bucket[i]),
        list_revenus_each_bucket[i][:10],
        len([k for k in list_revenus_each_bucket[i] if k < 0.01]),
        len([k for k in list_revenus_each_bucket[i] if k < 0.01])
        / len(list_revenus_each_bucket[i]),
    )
    # print([len(k) for k in list_revenus_each_bucket], sum([len(k) for k in list_revenus_each_bucket]))
    # Pour chaque bucket de rfr on instancie une classe qui va traiter les revenus du capital
    bdr = BucketDeVAR(
        frontieres_valeurs[i], frontieres_valeurs[i + 1], list_revenus_each_bucket[i]
    )
    # On converti la classe en dictionnaire pour l'exporter
    to_export += [bdr.to_dict()]

# On écrit le résultat
with open("ExportCopule.txt", "w") as f:
    f.write(repr(to_export))

"""
On a viré : calcul de la densité jointe (nous filait des résultats qui avaient l'air moins cools que le ExportCopule.csv

d={}

for seuil_revkire in col_name_to_seuils_copules["revkire"]:
    d[seuil_revkire]={}
    for seuil_rkname in col_name_to_seuils_copules[rkname]:
        d[seuil_revkire][seuil_rkname] = len(rfrs[(rfrs["revkire"]>=seuil_revkire) & (rfrs[rkname]>= seuil_rkname)])
        print(seuil_revkire, seuil_rkname, d[seuil_revkire][seuil_rkname])

print("Computed FdR jointe", time()-starttime)

print(*col_name_to_seuils_copules[rkname], sep=",")
for sr in col_name_to_seuils_copules["revkire"]:
    print(sr,*list(d[sr].values()), sep=",")

"""
##############################################################################
# Calcul des Fonctions de Répartition (FdR)
# (Indépendament les unes des autres, contrairement à ce qu'on a fait au dessus)
#################################
starttime = time()

nbff = len(rfrs)  # Nb de foyers fiscaux

rkname = "rev_capital_partiel"
""" Le revenu du capital est calculé à partir d'une partie de ses éléments:
Rk = SUM(z2cg,   # RCM avec RDS déjà prélevée
    z2dc, # Revenus ouvrant droit à abattement - Div
    z2tr, # Placement revenu fixe sans abattement
    z2bh, # 2TR déjà soumis PS
    z2ck, # CI prélèvement forfait Déjà verse
    z2dh, # Assurance - vie avec PL au taux 7,6%
    z2ch  # Revenus des contrats d'assurance-vie
    )
    On a choisi ces 7 colonnes de POTE car elles constituent 71M€, soit 95% du Revenu du Capital (~75M€)
    Voir declarations_2042_revenus_2018-correspondances.xlsx
     """

cols = [rkname, "revkire"]

# Les premiers seuils sont choisis tous les 1000, et on ajoute les seuils équivalents aux déciles de population (3714, 9168, etc..)
col_name_to_seuils = {
    "revkire": [
        0,
        1,
        1500,
        3714,
        5000,
        7000,
        9168,
        10000,
        12000,
        13500,
        15000,
        16000,
        17000,
        18000,
        19221,
        21000,
        23314,
        25000,
        28000,
        30000,
        33000,
        35000,
        37500,
        40000,
        42500,
        45000,
        47500,
        50000,
        60000,
        75000,
        100000,
    ],
    rkname: [
        0,
        1,
        1500,
        3714,
        5000,
        7000,
        9168,
        10000,
        12000,
        13500,
        15000,
        16000,
        17000,
        18000,
        19221,
        21000,
        23314,
        25000,
        28000,
        30000,
        33000,
        35000,
        37500,
        40000,
        42500,
        45000,
        47500,
        50000,
        60000,
        75000,
        100000,
    ],
}

for col in cols:
    # pas du tout optimal en temps de calcul mais bon
    # Les seuils ont vocation à offrir une granularité meilleure que l'existant.
    # On va faire à la main, je dirais qu'on a envie d'un peu plus de lignes mais pas forcément beaucoup.
    # Essayons de mettre :
    # - maximum 5% de la population dans chaque tranche
    # - Meilleure estimation des hauts revenus (on peut monter jusqu'à 20_000_000 de rfr sans mettre à mal le SS : il y a >100 FF au dessus de 20M de RFR.

    seuils = col_name_to_seuils[col]

    while (
        seuils[-1] < 20_000_000
    ):  # Du dernier seuil (100 k€) jusqu'à 20m€, on rajoute des seuils: 130k€, 169k€, ...
        seuils += [int(seuils[-1] * 1.3)]

    seuils += [
        50_000_000
    ]  # On sait que au dela de 50M€ on peut avoir un bucket qui respecte le secret statistiqu
    print("*" * 50 + "On calcule la fonction de répartition du " + col + "*" * 50)
    print(f"On a : {len(seuils)} seuils")

    # Initialisation
    pops = []
    sumrevs = []
    nombre_observations_tranche = (
        nbff  # C'est le nb d'observations (i.e. de foyers fiscaux)
    )
    min_pt = nbff
    max_poids = 0

    with open(f"CalibPOTE_{col}.txt", "w") as f:
        # On écrit trois colonnes pour observer la fonction de répartition de différents revenus (ici)
        f.write(
            "Rk,Nk,Ark\n"
        )  # Rk : Seuil de revenu, Nk : % de foyers au-dessus du seuil, Ark : Revenu moyen des foyers > Seuil

        for seuil in seuils:
            filtered_above_seuil = rfrs[rfrs[col] >= seuil]
            nb_above_seuil = len(
                filtered_above_seuil
            )  # Nb de foyers au-dessus du seuil
            sum_above_seuil = sum(
                filtered_above_seuil[col]
            )  # Somme des revenus > seuil
            pops += [
                nb_above_seuil
            ]  # La liste des Nb de foyers fiscaux au-dessus de chaque seuil
            sumrevs += [
                sum_above_seuil
            ]  # La liste des sommes de revenus de tous les ff > seuil
            highest_rev = seuil - 1

            if len(pops) >= 2:
                nombre_observations_tranche = pops[-2] - pops[-1]
            # print(sumrevs, highest_rfr)
            # On vérifie la part du plus gros revenu dans sa case (il doit être < 85%)
            poids_plus_gros_ff = (
                (highest_rev / (sumrevs[-2] - sumrevs[-1])) if highest_rev > 0 else 0
            )
            print(
                seuil,
                nb_above_seuil,
                nb_above_seuil / nbff,
                sum_above_seuil / nb_above_seuil,
                "nombres foyers tranches (doit être >12)",
                nombre_observations_tranche,
                "poids plus haut revenu (doit être <85%)",
                poids_plus_gros_ff,
            )

            min_pt = min(
                min_pt, nombre_observations_tranche
            )  # On regarde le plus petit nb d'observations / tranche
            max_poids = max(int(poids_plus_gros_ff), max_poids)
            f.write(
                f"{seuil},{nb_above_seuil / nbff},{sum_above_seuil / nb_above_seuil}\n"
            )

        print(
            "Population de la dernière case",
            pops[-1],
            "Poids de la plus grande observation dans la dernière case",
            rfrs[col].max() / sumrevs[-1],
        )
        min_pt = min(min_pt, pops[-1])
        max_poids = max(max_poids, rfrs[col].max() / sumrevs[-1])

    sys.stderr.write(
        f"Well done, elapsed = {time()-starttime : .2f}, smallest tranche : {min_pt}, poids maximum d'un FF dans une tranche {max_poids*100:.2f}%"
    )
