"""
This script compute the avant and plf (if exist) and save them in the database.
It is supposed to be run by the CI after deploy to ensure using last calculus.
"""

from Simulation_engine.simulate_pop_from_reform import (
    simulation,
    PERIOD,
    TBS_DEFAULT,
    DUMMY_DATA,
)
from models import to_postgres_from_df
import unittest
import warnings
from pandas.core.common import SettingWithCopyWarning  # type: ignore
from openfisca_core.warnings import MemoryConfigWarning, TempfileWarning  # type: ignore
tc = unittest.TestCase()


def generate_default_results():
    # precalcul cas de base sur la population pour le cache
    base_results = None
    liste_base_reformes = []
    for reforme in TBS_DEFAULT:
        print("XXXXXXXX Processing", reforme)
        liste_base_reformes += [reforme]
        bulk_data_simulation, data_by_entity = simulation(
            PERIOD, DUMMY_DATA, TBS_DEFAULT[reforme]
        )
        if base_results is None:
            base_results = data_by_entity["foyer_fiscal"][["wprm", "idfoy"]]
        base_results[reforme] = bulk_data_simulation.calculate("irpp", PERIOD)

    sum_av = (base_results['wprm'] * base_results['avant']).sum()
    print("Somme finale affichée d'impôt sur le revenu avant PLF", sum_av)
    # ########### REMETTRE EN PLACE A LA FIN DE LA PIPELINE
    # tc.assertGreater(sum_av * -1, 108 * 1e9)
    # tc.assertLess(sum_av * -1, 111 * 1e9)
    if 'plf' in base_results.columns:
        sum_ap = (base_results['wprm'] * base_results['plf']).sum()
        print("Somme finale affichée d'impôt sur le revenu avec le PLF", sum_ap)
        # ########### REMETTRE EN PLACE A LA FIN DE LA PIPELINE
        # tc.assertGreater(sum_ap * -1, 108 * 1e9)
        # tc.assertLess(sum_ap * -1, 111 * 1e9)

    return base_results[["idfoy"] + liste_base_reformes + ["wprm"]]


if __name__ == "__main__":
    table_name = "base_result_cache"
    print("Will update", table_name, "in database.")
    warnings.simplefilter(action="ignore", category=SettingWithCopyWarning)
    warnings.simplefilter(action="ignore", category=MemoryConfigWarning)
    warnings.simplefilter(action="ignore", category=TempfileWarning)
    df = generate_default_results()
    to_postgres_from_df(df, table_name, if_exists="replace")
