import pandas as pd  # type: ignore
from pathlib import Path
from utils.folder_finder import path_folder_assets  # type: ignore

# See DGCL data (.xlsx even when the filename looks like a .csv) for 2020 at:
# http://www.dotations-dgcl.interieur.gouv.fr/consultation/criteres_repartition.php

path_assets = path_folder_assets()

dict_convert_2020_to_2019 = {
    "Dotation de solidarité rurale - Cible - IS DSR Cible": "Dotation de solidarité rurale - Cible - Indice synthétique",
    "Dotation de solidarité rurale - Cible - Part Pfi": "Dotation de solidarité rurale - Cible - Part Pfi (avant garantie CN)",
    "Dotation de solidarité rurale - Cible - Part VOIRIE": "Dotation de solidarité rurale - Cible - Part VOIRIE (avant garantie CN)",
    "Dotation de solidarité rurale - Cible - Part ENFANTS": "Dotation de solidarité rurale - Cible - Part ENFANTS (avant garantie CN)",
    "Dotation de solidarité rurale - Cible - Part Pfi/hectare (Pfis)": "Dotation de solidarité rurale - Cible - Part Pfi/hectare (Pfis) (avant garantie CN)",
    "Dotation de solidarité rurale Bourg-centre - Appartenance à une agglomération avec le chef-lieu de département": "Dotation de solidarité rurale Bourg-centre - Chef-lieu de département agglo",
    "Informations générales - Population DGF de l'année N'": "Informations générales - Population DGF Année N'",
    "Informations générales - Strate démographique de l'année N": "Informations générales - Strate démographique Année N",
    "Informations générales - Population INSEE de l'année N": "Informations générales - Population INSEE Année N ",
    "Informations générales - Superficie année N": "Informations générales - Superficie 2019",
    "Dotation forfaitaire - Recettes réelles de fonctionnement des communes N-2 pour l'année N": "Dotation forfaitaire - Recettes réelles de fonctionnement des communes N-2 pour Année N",
}

dict_convert_2021_to_2019 = {
    "Dotation de solidarité rurale - Cible - IS DSR Cible": "Dotation de solidarité rurale - Cible - Indice synthétique",
    "Dotation de solidarité rurale - Cible - Part Pfi": "Dotation de solidarité rurale - Cible - Part Pfi (avant garantie CN)",
    "Dotation de solidarité rurale - Cible - Part VOIRIE": "Dotation de solidarité rurale - Cible - Part VOIRIE (avant garantie CN)",
    "Dotation de solidarité rurale - Cible - Part ENFANTS": "Dotation de solidarité rurale - Cible - Part ENFANTS (avant garantie CN)",
    "Dotation de solidarité rurale - Cible - Part Pfi/hectare (Pfis)": "Dotation de solidarité rurale - Cible - Part Pfi/hectare (Pfis) (avant garantie CN)",
    "Dotation de solidarité rurale Bourg-centre - Appartenance à une agglomération avec le chef-lieu de département": "Dotation de solidarité rurale Bourg-centre - Chef-lieu de département agglo",
    "Informations générales - Population DGF de l'année N": "Informations générales - Population DGF Année N'",
    "Informations générales - Strate démographique de l'année N": "Informations générales - Strate démographique Année N",
    "Informations générales - Population INSEE de l'année N": "Informations générales - Population INSEE Année N ",
    "Informations générales - Superficie de l'année N": "Informations générales - Superficie 2019",
    "Dotation forfaitaire - Recettes réelles de fonctionnement des communes N-2 pour l'année N": "Dotation forfaitaire - Recettes réelles de fonctionnement des communes N-2 pour Année N",
    "Dotation forfaitaire - Population DGF majorée de l'année N": "Dotation forfaitaire - Population DGF major�e",
    "Dotation de solidarité rurale Bourg-centre - Pourcentage de la population communale dans le canton d'appartenance en 2014": "Dotation de solidarité rurale Bourg-centre - Pourcentage de la population communale dans le canton",
    "Potentiel fiscal et financier des communes - Potentiel financier moyen par habitant de la strate": "Potentiel fiscal et financier des communes - Potentiel financier moyen de la strate",
}


def convert_col_names(data, dict_convert):
    # Modifies column names that changed between 2019 and 2020
    # We give them the name they had in 2019, cause our loader uses these.
    for ori, _ in dict_convert.items():
        if ori not in data.columns:
            print(f"WARNING: {ori} not found !!!")
    data = data.rename(columns=dict_convert, errors="raise")  # raise / ignore
    return data


def xlsxtocsv(filename):
    print(f"Loading {filename}")
    df = pd.read_excel(filename, dtype=str)  # Les noms de colonnes viennent sur 2 lignes dans le fichier DGCL.
    # On doit fusionner les noms de colonnes qui se trouvent sur les lignes 3 et 4
    col_name_separator = " - "
    new_cols = []
    for x, y in list(zip(df.iloc[1], df.iloc[2])):
        new_cols.append(f"{x}{col_name_separator}{y}")
    df.columns = new_cols
    # On efface la première colonne
    df.drop(['nan - nan'], axis=1, inplace=True)
    # On efface les trois premières lignes
    df.drop([0, 1, 2], axis=0, inplace=True)
    df = df.replace(r'\.', ",", regex=True)
    df = convert_col_names(df, dict_convert_2021_to_2019)
    out_file = f'{Path(filename).parent.resolve()}/{Path(filename).resolve().stem}.csv'
    print(f'Saving to {out_file}')
    df.to_csv(out_file, index=False, decimal=",")


if __name__ == "__main__":
    xlsxtocsv(path_assets + "/data/2021-communes-criteres-repartition.xlsx")
