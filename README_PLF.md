# Activation & Désactivation du mode PLF

Pour l'[application IR](http://leximpact.an.fr/ir) et l'[application dotations](http://leximpact.an.fr/dotations), il est possible d'activer un mode où, en plus de la loi en vigueur, un Projet de Loi de Finances (PLF) sert de base aux calculs d'amendements.

Cette documentation présente les éléments à mettre à jour pour activer et désactiver le mode `PLF`.

## Étapes communes activation & désactivation du PLF

Les résultats des simulateurs dépendent de deux facteurs :

* la réglementation employée (loi en vigueur, PLF, ...),
* la population à laquelle on applique la réglementation.

Pour l'IR :

* la population est stockée en base de données.
* et, on stocke également en base de donnée, des pré-calculs d'impact des réglementations sur la population afin d'économiser du temps de calcul (pré-calculs des effets de la loi en vigueur, d'un PLF mais pas d'amendements usagers).

Par conséquent, pour l'IR, toute évolution de la réglementation ou de la population nécessite la mise à jour de la base de données.

### [IR] Mise à jour de la population : `POPULATION_TABLE_PATH`

Les données de population employées par l'application IR s'appuient sur un extrait de l'enquête INSEE [ERFS](https://www.insee.fr/fr/metadonnees/source/serie/s1231)-FPR (Fichiers de Production et Recherche) adaptée aux besoins des requêtes de l'application.

[Cette documentation LexImpact (accès restreint)](https://cloud.leximpact.dev/index.php/f/1573) décrit les étapes de passage de la base `ERFS-FPR` à la base leximpact (Fichier `Générer des données à partir d_un fichier ERFS-FPR.docx`).

Puis, la base leximpact est intégrée à l'environnement de l'application IR. En particulier, la variable d'environnement `POPULATION_TABLE_PATH` est mise à jour et sa valeur est représente les données corrigées. 

La base de donnée doit être à jour de l'année de `base` de calcul (inflation etc.).  
C'est à dire que pour le PLF `N+1`, la base de donnée doit contenir une population représentative de l'année `N`.

Cette population est employée pour des calculs budgétaires sur les déciles. Sa mise à jour comprend en partie la prise en compte de l'évolution des déciles de population.

Voici deux exemples d'évolutions prises en comptes au moment du PLF :

* Au PLF 2021, la création de la base leximpact employait une publication du Sénat. 
* Au PLF 2022, LexImpact emploie la [base POTE](https://www.casd.eu/source/declarations-dimpot-sur-le-revenu-des-foyers-fiscaux-formulaire-2042-et-annexes/) pour extraire les déciles : 
    1. On a extrait de POTE sur le CASD, `CalibPote.txt` (.txt qui contient des données au format .csv) 
    2. On a renommé `CalibPote.txt` en `Simulation_engine/Calib/ResFinalCalibSenat.csv` par respect de la tradition.  
    3. On a ensuite mis à jour le dépôt comme cela a été réalisé dans cette [MR!70](https://git.leximpact.dev/leximpact/leximpact-server/-/merge_requests/70/) pour le PLF 2022. Noter alors que deux étapes de mise à jour existent : on distingue l'étape cas types de l'étape population.

### [IR] Mise à jour des résultats pré-calculés : `NAME_TABLE_BASE_RESULT`

Une table de la base de donnée doivent être mise à jour en cas d'évolution de la loi de référence :

* désactivation d'un PLF ou migration vers une nouvelle année de calcul : la migration vers une nouvelle année de calcul survient habituellement à la désactivation d'un PLF puisque le vote du PLF produit habituellement une évolution de la loi en vigueur.

* activation d'un PLF : des pré-calculs de PLF doivent être mis-à-jour afin de permettre, à chaque calcul d'amendement, de prendre les résultats de la loi actuelle et du PLF dans la base de données.

Cette mise à jour des pré-calculs de base correspond à la mise à jour de la table désignée par `NAME_TABLE_BASE_RESULT`.  
Au sein de l'environnement de LexImpact, une mise à jour automatique est implémentée en intégration continue (CI) pour faciliter ces mises à jour.

### [IR] Automatisme LexImpact : mise à jour des bases à chaque déploiement

La [CI gitlab](https://git.leximpact.dev/leximpact/leximpact-server/-/blob/934f1c18c34624a30c430d6ca5bb2871f1474382/.gitlab-ci.yml#L106) calcule `base_result_cache` à chaque déploiement pour éviter les désynchronisations entre les paramètres du `.env` (calcule `NAME_TABLE_BASE_RESULT` en fonction de `POPULATION_TABLE_PATH`) tout comme entre la table et l'éventuelle évolution d'`openfisca-france`.

Voici un exemple du script de mise à jour : [./scripts/update_cache.py](https://git.leximpact.dev/leximpact/leximpact-server/-/blob/dd9199ee5b661313eb507dd7fb58eeafe941e71b/scripts/update_cache.py)

### [IR] Configuration LexImpact : `POPULATION_TABLE_PATH` et `NAME_TABLE_BASE_RESULT`

Extrait du `.env` LexImpact fourni ici en tant qu'exemple concret de configuration :

```ini
NAME_TABLE_BASE_RESULT=base_result_cache
POPULATION_TABLE_PATH=data_erfs_2018_aged_to_2021_20210923
```

Sachant que `base_result_cache` est équivalent à `base_results` où le préfixe `_cache` a été ajouté pour préciser que ce sont des données pré-calculées.

## Activation d'un PLF, après mise à jour de la population

L'ajout d'un PLF nécessite une mise à jour du `.env`.

Extrait au PLF 2022 :

```ini
#RECETTES_ETAT_EURO="66_900_000_000" #Montant total sur lequel les résultats renvoyés seront ajustés en cas d'ajustement des résultats (cas par défaut). La valeur renseignée ici sera celle qui sera affichée pour les recettes de l'Etat totales avec le code existant. 
YEAR_COMPUTATION="2021" #Année où s'opère le calcul du code existant et des réformes.

#PLF_PATH="reformes.reformePLF_2021.reforme_PLF_2021" #Contient l'adresse où l'on peut trouver un dictionnaire représentant le PLF. Commenter cette variable pour désactiver le calcul du PLF.
PLF_PATH="reformes.reformePLF_2022.reforme_PLF_2022"

IGNORE_WARNING=yes
```

`RECETTES_ETAT_EURO` était initialement dans le `.env`. Il est maintenant inutile dans ce fichier parce que l'historique des valeurs a été inscrit en dur dans `Simulation_engine/simulate_pop_from_reform.py`.

Changement de `RECETTES_ETAT_EURO`, voir :

* [MR server!71 qui déplace l'information dans Simulation_engine/simulate_pop_from_reform.py](https://git.leximpact.dev/leximpact/leximpact-server/-/merge_requests/71)

Exemple d'ajout du PLF 2022 avec la [MR!69](https://git.leximpact.dev/leximpact/leximpact-server/-/merge_requests/69) (où `YEAR_COMPUTATION` était en retard).

## Désactivation d'un PLF

Il s'agit de désactiver le descriptif du PLF et de mettre à jour l'année de calcul.  

Les données pour les calculs budgétaires doivent être cohérentes avec cette évolution :

* `POPULATION_TABLE_PATH` doit refléter une population de la nouvelle année de calcul.
  > Ici, la meilleure période de mise à jour n'a pas encore été formalisée : 
  > quelques mois après le passage à la nouvelle année ? Au 2ème trimestre ?
* `NAME_TABLE_BASE_RESULT` doit contenir les données pré-calculées sur la nouvelle version du modèle `openfisca-france`. Pour LexImapct, ce traitement est automatisé en CI.

Au-delà des variables `POPULATION_TABLE_PATH` et `NAME_TABLE_BASE_RESULT`, voici un extrait de `.env` mis à jour suite à l'adoption du PLF 2022 et ainsi migré vers l'année 2022 :

```ini
YEAR_COMPUTATION="2022" 
# Année où s'opère le calcul du code existant et des réformes.

# 2022 - PLF_PATH="reformes.reformePLF_2022.reforme_PLF_2022"
# Contient l'adresse où l'on peut trouver un dictionnaire représentant le PLF. Commenter cette variable pour désactiver le calcul du PLF.
# Et, commenter pour les tests.

# RECETTES_ETAT_EURO="82_400_000_000"
# Recettes prévisionnelles indiquées dans le PLF 2022.
```

Ici :

* `YEAR_COMPUTATION` a été incrémenté de 1 année
* `PLF_PATH` a été commenté
* `RECETTES_ETAT_EURO` reste identique à sa valeur durant le PLF. Il est commenté car il était initialement dans le `.env` mais il est maintenant inutile dans ce fichier :  l'historique des valeurs a été inscrit en dur dans `Simulation_engine/simulate_pop_from_reform.py`.

> Sachant que l'historique des valeurs de `RECETTES_ETAT_EURO` provient des PLF successifs où les valeurs les plus récentes sont publiées. Les valeurs anciennes pourraient à l'avenir être migrées vers les valeurs effectives d'impôt indiquées, par exemple, dans le [Mémo IR](https://documentation.leximpact.dev/leximpact_prepare_data/memo_irpp).

Et, _voilà_ il n'y a plus qu'à appliquer le tout en [pré-]production ! 😁
> Du côté des environnements de production LexImpact, la [documentation ops dédiée à leximpact-server (accès restreint)](https://cloud.leximpact.dev/index.php/f/13430) est là pour accompagner cette transition.
