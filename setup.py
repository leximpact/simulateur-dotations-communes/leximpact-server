from setuptools import setup, find_packages  # type: ignore

basic_requirements = [
    "alembic >= 1.0.11, < 2.0.0",
    "connexion[swagger-ui] >= 2.6.0, < 3.0.0",
    "flask-cors >= 3.0.7, < 3.1.0",
    "gunicorn >= 20.0.0, < 21.0.0",
    "pandas >= 1.3.3, < 1.4.0",
    "psycopg2-binary >= 2.8.3, < 3.0.0",
    "pyjwt >= 1.7.1, < 2.0.0",
    "python-dotenv >= 0.10.3, < 1.0.0",
    "sqlalchemy >= 1.4.15, < 2.0.0",
    "toolz >= 0.9.0, < 1.0.0",
]

impot_revenu_requirements = [
    "openfisca-france >= 116.0.0, < 117.0.0",
]

dotations_requirements = [
    "openfisca-france-dotations-locales >= 0.7.0, < 1.0.0",
]

setup(
    name="LexImpact Server",
    author="LexImpact Team",
    author_email="leximpact@openfisca.org",
    version="2.0.0",
    license="https://www.fsf.org/licensing/licenses/agpl-3.0.html",
    url="https://git.leximpact.dev/leximpact/leximpact-server/",
    classifiers=[
        "Development Status :: 4 - Beta",
        "Environment :: Web Environment",
        "License :: OSI Approved :: GNU Affero General Public License v3",
        "Natural Language :: French",
        "Operating System :: Microsoft :: Windows",
        "Operating System :: POSIX",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3.7",
        "Topic :: Scientific/Engineering :: Information Analysis",
    ],
    install_requires=[
        basic_requirements,
        impot_revenu_requirements,
        dotations_requirements
    ],
    extras_require={
        "dev": [
            "flake8 >= 4.0.1",
            "flake8-bugbear >= 22.3.23",
            # "mock", mock is now part of the Python standard library, available as unittest.mock in Python 3.3 onwards.
            "mypy >= 0.942",
            "pytest >= 4.4.1",
            "pytest-mock",  # Not imported but needed to use mocker
            "tables >= 3.7.0",  # Pour HDF5
            "types-mock",
            "jupyter >= 1.0.0, < 2.0.0",
        ],
        "dev-advanced": [
            "snakeviz",  # Only used for cprof debugging
            "tables >= 3.7.0",  # Only used for HDF5
            "openpyxl >= ^3.0.8, < 3.1.0",  # Nécessaire uniquement pour scripts/convert_dgcl_xlsx_to_csv.py pour les dotations.
            "jupyterlab",  # Pour Notebook Dotation
        ],
    },
    packages=find_packages(exclude=["tests*"]),
)
